<?php

namespace    Dcms\Conditions;

/**
 *
 * @author web <web@groupdc.be>
 */
use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;

class ConditionsServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    public function boot()
    {
        $this->loadViewsFrom(realpath(__DIR__ . '/resources/views'), 'dcmsconditions');
        $this->setupRoutes($this->app->router);
        // this  for conig
        $this->publishes([
            __DIR__ . '/config/dcms_sidebar.php' => config_path('Dcms/Conditions/dcms_sidebar.php'),
        ]);

        if (!is_null(config('dcms.conditions'))) {
            $this->app['config']['dcms_sidebar'] = array_merge((array) $this->app["config"]["dcms_sidebar"], config('dcms.conditions.dcms_sidebar'));
        }
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router $router
     *
     * @return void
     */
    public function setupRoutes(Router $router)
    {
        $router->group(['namespace' => 'Dcms\Conditions\Http\Controllers'], function ($router) {
            require __DIR__ . '/Http/web.php';
        });
    }

    public function register()
    {
    }
}
