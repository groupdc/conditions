<?php

return [
    "Conditions" => [
        "icon"  => "fa-bug",
        "links" => [
            ["route" => "admin/conditions", "label" => "Conditions", "permission" => "conditions"],
            ["route" => "admin/conditions/categories", "label" => "Categories", "permission" => "conditions"],
        ],
    ],
];
