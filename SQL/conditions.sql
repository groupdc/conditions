/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table conditions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `conditions`;

CREATE TABLE `conditions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(36) DEFAULT NULL,
  `conditions_type_id` int(10) unsigned DEFAULT NULL,
  `conditions_category_id` int(10) unsigned DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `planting` tinyint(4) DEFAULT '0',
  `nursing` tinyint(4) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DELIMITER ;;
/*!50003 SET SESSION SQL_MODE="ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`homestead`@`%` */ /*!50003 TRIGGER `before_insert_conditions` BEFORE INSERT ON `conditions` FOR EACH ROW BEGIN
    SET new.uuid = uuid();
END */;;
DELIMITER ;
/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;


# Dump of table conditions_category
# ------------------------------------------------------------

DROP TABLE IF EXISTS `conditions_category`;

CREATE TABLE `conditions_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table conditions_category_language
# ------------------------------------------------------------

DROP TABLE IF EXISTS `conditions_category_language`;

CREATE TABLE `conditions_category_language` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `conditions_category_id` int(10) unsigned DEFAULT NULL,
  `language_id` int(11) unsigned DEFAULT NULL,
  `category` varchar(150) DEFAULT NULL,
  `slug` varchar(150) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table conditions_language
# ------------------------------------------------------------

DROP TABLE IF EXISTS `conditions_language`;

CREATE TABLE `conditions_language` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `language_id` int(10) unsigned DEFAULT NULL,
  `conditions_id` int(11) unsigned DEFAULT NULL,
  `condition` varchar(150) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `slug` varchar(150) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table conditions_to_advices
# ------------------------------------------------------------

DROP TABLE IF EXISTS `conditions_to_advices`;

CREATE TABLE `conditions_to_advices` (
  `conditions_id` int(11) unsigned NOT NULL,
  `advices_id` int(11) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`conditions_id`,`advices_id`),
  CONSTRAINT `conditions_to_advices_ibfk_1` FOREIGN KEY (`conditions_id`) REFERENCES `conditions` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table conditions_to_plants
# ------------------------------------------------------------

DROP TABLE IF EXISTS `conditions_to_plants`;

CREATE TABLE `conditions_to_plants` (
  `conditions_id` int(11) unsigned NOT NULL,
  `plants_id` int(11) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`conditions_id`,`plants_id`),
  KEY `FK_plants_id` (`plants_id`),
  CONSTRAINT `FK_conditions_id` FOREIGN KEY (`conditions_id`) REFERENCES `conditions` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_plants_id` FOREIGN KEY (`plants_id`) REFERENCES `plants` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table conditions_to_products
# ------------------------------------------------------------

DROP TABLE IF EXISTS `conditions_to_products`;

CREATE TABLE `conditions_to_products` (
  `conditions_id` int(11) unsigned NOT NULL,
  `products_id` int(11) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`conditions_id`,`products_id`),
  KEY `FK_products_id` (`products_id`),
  CONSTRAINT `FK_conditions_to_products_conditions_id` FOREIGN KEY (`conditions_id`) REFERENCES `conditions` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_conditions_to_products_products_id` FOREIGN KEY (`products_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table conditions_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `conditions_type`;

CREATE TABLE `conditions_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table conditions_type_language
# ------------------------------------------------------------

DROP TABLE IF EXISTS `conditions_type_language`;

CREATE TABLE `conditions_type_language` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `conditions_type_id` int(10) unsigned DEFAULT NULL,
  `language_id` int(11) unsigned DEFAULT NULL,
  `type` varchar(150) DEFAULT NULL,
  `slug` varchar(150) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
