<?php

namespace Dcms\Conditions\Models;

use Dcms\Core\Models\EloquentDefaults;

class Conditions extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table = 'conditions';

    public function detail()
    {
        return $this->hasMany('Dcms\Conditions\Models\Conditiondetail', 'conditions_id', 'id');
    }

    public function category()
    {
        //return $this->belongsTo("Dcweb\Dcms\Models\Articles\Category", 'article_category_id', 'id');
    }

    public function productinformation()
    {
        //// BelongsToMany belongsToMany(string $related, string $table = null, string $foreignKey = null, string $otherKey = null, string $relation = null)
        //	return $this->belongsToMany('\Dcweb\Dcms\Models\Products\Information', 'articles_to_products_information_group',  'article_id','information_id');
    }

    public function plants()
    {
        /*
        The first argument in belongsToMany() is the name of the class Productdata, the second argument is the name of the pivot table, followed by the name of the product_id column, and at last the name of the product_data_id column.
        */
        return $this->belongsToMany('Dcms\Plants\Models\Plant', 'conditions_to_plants', 'conditions_id', 'plants_id');
    }
}
