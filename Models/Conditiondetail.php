<?php

namespace Dcms\Conditions\Models;

use Dcms\Core\Models\EloquentDefaults;

class Conditiondetail extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table = 'conditions_language';
}
