<?php

namespace Dcms\Conditions\Models;

use Dcms\Core\Models\EloquentDefaults;

class ConditionCategory extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table = 'conditions_category';

    public function detail()
    {
        return $this->hasMany('Dcms\Conditions\Models\Conditiondetail', 'conditions_id', 'id');
    }
}
