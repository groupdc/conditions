<?php

namespace Dcms\Conditions\Models;

use Dcms\Core\Models\EloquentDefaults;

class ConditionCategorydetail extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table = 'conditions_category_language';

    public function conditioncategory()
    {
        return $this->hasOne('Dcms\Conditions\Models\ConditionCategory', 'conditions_category_id', 'id');
    }
}
