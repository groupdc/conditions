<?php

namespace Dcms\Conditions\Models;

use Dcms\Core\Models\EloquentDefaults;

class ConditionsType extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table = 'conditions_type';

    public function detail()
    {
        return $this->hasMany('Dcms\Conditions\Models\ConditionTypeLanguage', 'conditions__type_id', 'id');
    }
}
