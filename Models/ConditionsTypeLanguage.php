<?php

namespace Dcms\Conditions\Models;

use Dcms\Core\Models\EloquentDefaults;

class ConditionsTypeLanguage extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table = 'conditions_type_language';

    public function type()
    {
        return $this->hasOne('Dcms\Conditions\Models\ConditionsType', 'conditions_type_id', 'id');
    }
}
