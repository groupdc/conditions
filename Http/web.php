<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Route::group(['middleware' => ['web']], function () {
    Route::group(["as" => "admin.", "prefix" => "admin"], function () {
        Route::group(["middleware" => "auth:dcms"], function () {

            //Conditions
            Route::group(["prefix" => "conditions", "as" => "conditions."], function () {
                Route::any("api/table", ["as" => "api.table", "uses" => "ConditionController@getDatatable"]);
                Route::group(["prefix" => "categories", "as" => "categories."], function () {
                    Route::any("api/table", ["as" => "api.table", "uses" => "ConditioncategoryController@getDatatable"]);
                });
                Route::resource("categories", "ConditioncategoryController");
                Route::any("api/plantrelationtable/{plant_id?}", ["as" => "api.plantrelationtable", "uses" => "ConditionController@getPlantRelationTable"]);
                Route::any("api/productrelationtable/{plant_id?}", ["as" => "api.productrelationtable", "uses" => "ConditionController@getProductRelationTable"]);
            });
            Route::resource("conditions", "ConditionController");
        });
    });
});
