<?php

namespace Dcms\Conditions\Http\Controllers;

use Dcms\Conditions\Models\ConditionCategory;
use Dcms\Conditions\Models\ConditionCategorydetail;
use Dcms\Core\Http\Controllers\BaseController;
use Illuminate\Support\Str;
use View;
use Input;
use Session;
use Validator;
use Redirect;
use DB;
use DataTables;
use Auth;

class ConditioncategoryController extends BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // load the view and pass the categories
        return View::make('dcmsconditions::conditionscategory/index');
    }

    public function getDatatable()
    {
        $query = DB::connection('project')
            ->table('conditions_category')
            ->select(
                "conditions_category.id as id",
                "conditions_category_language.category",
                "conditions_category_language.id as conditioncategory_language_id",
                (DB::connection("project")->raw('Concat("<img src=\'/packages/Dcms/Core/images/flag-",lcase(country),".svg\' style=\'width:16px; height:auto;\'>") as country'))
            )
            ->join('conditions_category_language', 'conditions_category.id', '=', 'conditions_category_language.conditions_category_id')
            ->leftjoin('languages', 'conditions_category_language.language_id', '=', 'languages.id')
            ->orderBy('category');

        if (intval(session('overrule_default_by_language_id')) > 0) {
            $query->where('conditions_category_language.language_id', session('overrule_default_by_language_id'));
        }
    
        return DataTables::queryBuilder($query)
            ->addColumn('edit', function ($model) {
                return '<form method="POST" action="/admin/conditions/categories/' . $model->conditioncategory_language_id . '" accept-charset="UTF-8" class="pull-right"> <input name="_token" type="hidden" value="' . csrf_token() . '"> <input name="_method" type="hidden" value="DELETE">
																											<a class="btn btn-xs btn-default" href="/admin/conditions/categories/' . $model->id . '/edit"><i class="far fa-pencil"></i></a>
																											<button class="btn btn-xs btn-default" type="submit" value="Delete this plant" onclick="if(!confirm(\'Are you sure to delete this item?\')){return false;};"><i class="far fa-trash-alt"></i></button>
																								</form>';
            })
            ->rawColumns(['country', 'edit'])
            ->make(true);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $languages = DB::connection("project")
            ->table("languages")
            ->select((DB::connection("project")->raw("'' as condition_category_detailid, '' as category")), "id", "id as language_id", "language", "country", "language_name")->get();

        // load the create form (app/views/categories/create.blade.php)
        return View::make('dcmsconditions::conditionscategory/form')
            ->with('languages', $languages);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        //	get the category
        $ConditionCategory = ConditionCategory::find($id);

        $languages = DB::connection("project")->select('
													SELECT language_id, languages.language, languages.country, languages.language_name,
													conditions_category_language.id as condition_category_detailid, conditions_category_language.conditions_category_id, conditions_category_language.category
													FROM conditions_category_language
													LEFT JOIN languages on languages.id = conditions_category_language.language_id
													WHERE  languages.id is not null AND  conditions_category_id = ?
													UNION
													SELECT languages.id , language, country, language_name,
													0 , 0 , \'\'
													FROM languages
													WHERE id NOT IN (SELECT language_id FROM conditions_category_language WHERE conditions_category_id = ?) ORDER BY 1
													', [$id, $id]);

        return View::make('dcmsconditions::conditionscategory/form')
            ->with('ConditionCategory', $ConditionCategory)
            ->with('languages', $languages);
    }

    private function validateConditioncategoryForm()
    {
        return true;
    }

    private function saveConditioncategoryProperties($conditioncategoryid = null)
    {
        // do check if the given id is existing.
        if (!is_null($conditioncategoryid) && intval($conditioncategoryid) > 0) {
            $ConditionCategory = ConditionCategory::find($conditioncategoryid);
        }

        if (!isset($ConditionCategory) || is_null($ConditionCategory)) {
            $ConditionCategory = new ConditionCategory;
        }

        $ConditionCategory->save();

        return $ConditionCategory;
    }

    private function saveConditioncategoryDetail(ConditionCategory $ConditionCategory, $givenlanguage_id = null)
    {
        $input = request()->all();

        $Conditioncategorydetail = null;

        foreach ($input["category"] as $language_id => $title) {
            if ((is_null($givenlanguage_id) || ($language_id == $givenlanguage_id)) && (strlen(trim($input['category'][$language_id])) > 0)) {
                $Conditioncategorydetail = null; // reset when in a loop
                $newDetail = true;

                if (intval($input["condition_category_detailid"][$language_id]) > 0) {
                    $Conditioncategorydetail = ConditionCategorydetail::find($input["condition_category_detailid"][$language_id]);
                }
                if (!isset($Conditioncategorydetail) || is_null($Conditioncategorydetail)) {
                    $Conditioncategorydetail = new ConditionCategorydetail;
                } else {
                    $newDetail = false;
                }

                $Conditioncategorydetail->language_id = $language_id;
                $Conditioncategorydetail->conditions_category_id = $ConditionCategory->id;
                $Conditioncategorydetail->category = $input["category"][$language_id];
                $Conditioncategorydetail->slug = Str::slug($input["category"][$language_id]);

                $Conditioncategorydetail->save();
            } elseif (isset($input["condition_category_detailid"][$language_id]) && intval($input["condition_category_detailid"][$language_id]) > 0) {
                $this->destroydetail($input["condition_category_detailid"][$language_id]);
            }
        }

        return $Conditioncategorydetail;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        if ($this->validateConditioncategoryForm() === true) {
            $Condition = $this->saveConditioncategoryProperties();
            $this->saveConditioncategoryDetail($Condition);

            // redirect
            Session::flash('message', 'Successfully created Condition category!');

            return Redirect::to('admin/conditions/categories');
        } else {
            return $this->validateConditioncategoryForm();
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        if ($this->validateConditioncategoryForm() === true) {
            $Condition = $this->saveConditioncategoryProperties($id);
            $this->saveConditioncategoryDetail($Condition);

            // redirect
            Session::flash('message', 'Successfully updated Condition category!');

            return Redirect::to('admin/conditions/categories');
        } else {
            return $this->validateConditioncategoryForm();
        }
    }

    public function destroydetail($id)
    {
        ConditionCategorydetail::destroy($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        // delete
        $this->destroydetail($id);

        // redirect
        Session::flash('message', 'Successfully deleted the Condition category!');

        return Redirect::to('admin/conditions/categories');
    }
}
