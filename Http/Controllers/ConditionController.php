<?php
namespace Dcms\Conditions\Http\Controllers;

use Dcms\Conditions\Models\ConditionCategorydetail;
use Dcms\Conditions\Models\Conditiondetail;
use Dcms\Conditions\Models\Conditions;
use Dcms\Conditions\Models\ConditionsType;
use Dcms\Conditions\Models\ConditionsTypeLanguage;
use Dcms\Products\Models\Product;
use Illuminate\Routing\Controller;
use Illuminate\Support\Str;
use View;
use Input;
use Session;
use Validator;
use Redirect;
use DB;
use DataTables;
use Auth;

class ConditionController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:conditions-browse')->only('index');
        $this->middleware('permission:conditions-add')->only(['create', 'store']);
        $this->middleware('permission:conditions-edit')->only(['edit', 'update']);
        $this->middleware('permission:conditions-delete')->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // load the view and pass the categories
        return View::make('dcmsconditions::conditions/index');
    }

    public function getDatatable()
    {
        $query = DB::connection('project')
                                ->table('conditions')
                                ->select(
                                    "conditions.id as id",
                                    "conditions_language.condition",
                                    "conditions_language.id as condition_language_id",
                                    (DB::connection("project")->raw('Concat("<img src=\'/packages/Dcms/Core/images/flag-",lcase(country),".svg\' style=\'width:16px; height:auto;\'>") as country'))
                                )
                                ->join('conditions_language', 'conditions.id', '=', 'conditions_language.conditions_id')
                                ->leftjoin('languages', 'conditions_language.language_id', '=', 'languages.id')
                                ->orderBy('condition');
        if (intval(session('overrule_default_by_language_id')) > 0) {
            $query->where('conditions_language.language_id', session('overrule_default_by_language_id'));
        }

        return DataTables::queryBuilder($query)
                                    ->addColumn('edit', function ($model) {
                                        $edit = '<form method="POST" action="/admin/conditions/'.$model->condition_language_id.'" accept-charset="UTF-8" class="pull-right"> <input name="_token" type="hidden" value="'.csrf_token().'"> <input name="_method" type="hidden" value="DELETE">';
                                        if (Auth::user()->can('conditions-edit')) {
                                            $edit .= '<a class="btn btn-xs btn-default" href="/admin/conditions/' . $model->id. '/edit"><i class="far fa-pencil"></i></a>';
                                        }
                                        if (Auth::user()->can('conditions-delete')) {
                                            $edit .= '<button class="btn btn-xs btn-default" type="submit" value="Delete this plant" onclick="if(!confirm(\'Are you sure to delete this item ? \')){return false;};"><i class="far fa-trash-alt"></i></button>';
                                        }

                                        $edit .= '</form>';
                                        return $edit;
                                    })
                                    ->rawColumns(['country','edit'])
                                    ->make(true);
    }

    public function getTypes($returnType = "array")
    {
        $mTypes = ConditionsTypeLanguage::where('language_id', '=', '1')->get();//->orderBy('category');
        if ($returnType == "model") {
            return $mTypes;
        }

        $aTypes = array();
        foreach ($mTypes as $Type) {
            $aTypes[$Type->conditions_type_id] = $Type->type;
        }

        return $aTypes;
    }

    public function getCategories($returnType = "array")
    {
        $mCategories = ConditionCategorydetail::where('language_id', '=', '1')->get();//->orderBy('category');
        if ($returnType == "model") {
            return $mCategories;
        }

        $aCategories = array();
        foreach ($mCategories as $Category) {
            $aCategories[$Category->conditions_category_id] = $Category->category;
        }

        return $aCategories;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $languages = DB::connection("project")
                                        ->table("languages")
                                        ->select((DB::connection("project")->raw("'' as conditions_detail_id, '' as `condition`, '' as `description`")), "id", "id as language_id", "language", "country", "language_name")->get();

        // load the create form (app/views/categories/create.blade.php)
        return View::make('dcmsconditions::conditions/form')
            ->with('languages', $languages)
            ->with('aCategories', $this->getCategories())
            ->with('aTypes', $this->getTypes());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //	get the category
        $Condition = Conditions::find($id);
        $languages = DB::connection("project")->select('
													SELECT language_id, languages.language, languages.country, languages.language_name, conditions.conditions_category_id, conditions_language.id as conditions_detail_id, conditions_language.conditions_id, conditions_language.condition, conditions_language.description
													FROM conditions_language
													LEFT JOIN conditions on conditions_language.conditions_id = conditions.id
													LEFT JOIN languages on languages.id = conditions_language.language_id
													WHERE  languages.id is not null AND  conditions_id = ?
													UNION
													SELECT languages.id , language, country, language_name, 0 , 0 , 0, \'\', \'\'
													FROM languages
													WHERE id NOT IN (SELECT language_id FROM conditions_language WHERE conditions_id = ?) ORDER BY 1
													', array($id,$id));

        return View::make('dcmsconditions::conditions/form')
                    ->with('Condition', $Condition)
                    ->with('languages', $languages)
                    ->with('aCategories', $this->getCategories())
                    ->with('aTypes', $this->getTypes());
    }

    private function validateConditionForm()
    {
        $rules = ['condition_Category_id' => 'integer|min:1'];
        $validator = Validator::make(request()->all(), $rules);

        if ($validator->fails()) {
            return redirect()
                                ->back()
                                ->withErrors($validator)
                                ->withInput();
        }
        return true;
    }

    private function saveConditionProperties($conditionid = null)
    {
        $newCondition = true;
        // do check if the given id is existing.
        if (!is_null($conditionid) && intval($conditionid)>0) {
            $Condition = Conditions::find($conditionid);
        }

        if (!isset($Condition) || is_null($Condition)) {
            $Condition = new Conditions;
        }

        $Condition->conditions_type_id = request()->get('conditions_type_id');
        $Condition->conditions_category_id = request()->get('conditions_category_id');
        $Condition->planting = intval(request()->get('planting'));
        $Condition->nursing = intval(request()->get('nursing'));
        $Condition->image = request()->get('image');
        $Condition->save();

        return $Condition;
    }

    private function saveConditionDetail(Conditions $Condition, $givenlanguage_id = null)
    {
        $input = request()->all();
        $Conditiondetail = null;

        foreach ($input["condition"] as $language_id => $title) {
            if ((is_null($givenlanguage_id) || ($language_id == $givenlanguage_id)) &&  (strlen(trim($input["condition"][$language_id]))>0)) {
                $Conditiondetail = null; // reset when in a loop
                $newDetail = true;

                if (intval($input["condition_detail_id"][$language_id]) > 0) {
                    $Conditiondetail = Conditiondetail::find($input["condition_detail_id"][$language_id]);
                }
                if (!isset($Conditiondetail) || is_null($Conditiondetail)) {
                    $Conditiondetail = new Conditiondetail;
                } else {
                    $newDetail = false;
                }

                $Conditiondetail->language_id 	= $language_id;
                $Conditiondetail->conditions_id	= $Condition->id;
                $Conditiondetail->condition 	= $input["condition"][$language_id];
                $Conditiondetail->description	= $input["description"][$language_id];
                $Conditiondetail->slug 			= Str::slug($input["condition"][$language_id]);

                $Conditiondetail->save();
            } elseif (isset($input["condition_detail_id"][$language_id]) && intval($input["condition_detail_id"][$language_id])>0) {
                $this->destroydetail($input["condition_detail_id"][$language_id]);
            }
        }

        return $Conditiondetail;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        if ($this->validateConditionForm() === true) {
            $Condition = $this->saveConditionProperties();
            $this->saveConditionDetail($Condition);

            // redirect
            Session::flash('message', 'Successfully created Condition!');
            return Redirect::to('admin/conditions');
        } else {
            return  $this->validateConditionForm();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        if ($this->validateConditionForm() === true) {
            $Condition =	$this->saveConditionProperties($id);
            $this->saveConditionDetail($Condition);

            // redirect
            Session::flash('message', 'Successfully updated condition!');
            return Redirect::to('admin/conditions');
        } else {
            return  $this->validateConditionForm();
        }
    }

    public function destroydetail($id)
    {
        Conditiondetail::destroy($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        // delete
        $this->destroydetail($id);
        
        // redirect
        Session::flash('message', 'Successfully deleted the condition!');
        return Redirect::to('admin/conditions');
    }

    /**
     * get the data for DataTable JS plugin.
     *
     * @return Response
     */
    public function getPlantRelationTable($plant_id = 0)
    {
        $query = DB::connection('project')
                                        ->table('conditions')
                                        ->select(
                                            (
                                                DB::connection("project")->raw('
                                                                    conditions.id,
                                                                    conditions_language.condition as `condition`,
																	case when (select count(*) from conditions_to_plants where conditions_to_plants.conditions_id = conditions.id and plants_id = "'.$plant_id.'") > 0 then 1 else 0 end as checked
																')
                                            )
                                        )->leftjoin('conditions_language', 'conditions.id', '=', 'conditions_language.conditions_id')
                                            //->whereNotNull('x.information_group_id')
                                            ->where('conditions_language.language_id', '=', '1')
                                            ->orderBy("language_id")
                                            ->orderBy('checked', 'DESC');

        return DataTables::queryBuilder($query)
                    //	->setNoGroupByOnCount(false)
                        ->addColumn('radio', function ($model) {
                            return '<input type="checkbox" name="condition_id[]" value="'.$model->id.'" '.($model->checked == 1?'checked="checked"':'').' id="chkbox_'.$model->id.'" > ';
                        })
                        ->rawColumns(['radio'])
                        ->make(true);
    }

    /**
     * get the data for DataTable JS plugin.
     *
     * @return Response
     */
    public function getProductRelationTable($product_id = 0)
    {
        if ($product_id) {
            $x = Product::find($product_id)->information->first();
            $information_group_id = 0;
            if (!is_null($x)) {
                $information_group_id = $x->information_group_id;
            }
        } else {
            $information_group_id = 0;
        }

        $query = DB::connection('project')
            ->table('conditions')
            ->select(
                (
                    DB::connection("project")->raw('
                        conditions.id,
                        conditions_language.condition as `condition`,
                        case when (select count(*) from products_information_group_to_conditions where products_information_group_to_conditions.conditions_id = conditions.id and information_group_id = "'.$information_group_id.'") > 0 then 1 else 0 end as checked
                    ')
                )
            )->leftjoin('conditions_language', 'conditions.id', '=', 'conditions_language.conditions_id')
            ->where('conditions_language.language_id', '=', '1')
            ->orderBy("language_id");
        /*
        if (intval(session('overrule_default_by_language_id')) > 0) {
                    $query->where('conditions_language.language_id', session('overrule_default_by_language_id'));
                }
                */

        return DataTables::queryBuilder($query)
            //	->setNoGroupByOnCount(false)
            ->addColumn('radio', function ($model) {
                return '<input type="checkbox" name="condition_id[]" value="'.$model->id.'" '.($model->checked == 1?'checked="checked"':'').' id="chkbox_'.$model->id.'" > ';
            })
            ->rawColumns(['radio'])
            ->make(true);
    }
}
