@extends("dcms::template/layout")

@section("content")

    <div class="main-header">
        <h1>Conditions</h1>
        <ol class="breadcrumb">
            <li><a href="{!! URL::to('admin/dashboard') !!}"><i class="far fa-tachometer-alt-average"></i> Dashboard</a></li>
            <li><a href="{!! URL::to('admin/conditions/categories') !!}"><i class="far fa-tags"></i> Conditions Categories</a></li>
            @if(isset($ConditionCategory))
                <li class="active">Edit</li>
            @else
                <li class="active">Create</li>
            @endif
        </ol>
    </div>

    <div class="main-content">

        @if(isset($ConditionCategory))
            {!! Form::model($ConditionCategory, array('route' => array('admin.conditions.categories.update', $ConditionCategory->id), 'method' => 'PUT')) !!}
        @else
            {!! Form::open(array('url' => 'admin/conditions/categories')) !!}
        @endif

        <div class="row">
            <div class="col-md-12">
                <div class="main-content-tab tab-container">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#information" role="tab" data-toggle="tab">Information</a></li>
                    </ul>

                    <div class="tab-content">
                        <div id="information" class="tab-pane active">

                            @if($errors->any())
                                <div class="alert alert-danger">{!! HTML::ul($errors->all()) !!}</div>
                            @endif

                            @if(isset($languages))
                                <ul class="nav nav-tabs" role="tablist">
                                    @foreach($languages as $key => $language)
                                        <li class="{!! (( intval(session('overrule_default_by_language_id')) == intval($language->language_id)) ? 'active' : ((intval(session('overrule_default_by_language_id')) == 0 && $key == 0) ? 'active': '') ) !!}"><a href="{!! '#' . $language->language . '-' . $language->country !!}" role="tab"
                                                                                           data-toggle="tab"><img
                                                        src="{!! asset('/packages/Dcms/Core/images/flag-' . strtolower($language->country) . '.svg') !!}" width="12"
                                                        height="12"/> {!! $language->language_name !!}</a></li>
                                    @endforeach
                                </ul>

                                <div class="tab-content">
                                    @foreach($languages as $key => $information)
                                        <div id="{!! $information->language . '-' . $information->country !!}" class="tab-pane {!! (( intval(session('overrule_default_by_language_id')) == intval($information->language_id)) ? 'active' : ((intval(session('overrule_default_by_language_id')) == 0 && $key == 0) ? 'active': '')) !!}">
                                            {!!Form::hidden('condition_category_detailid[' . $information->language_id . ']', $information->condition_category_detailid) !!}
                                            <div class="form-group">
                                                {!! Form::label('category[' . $information->language_id . ']', 'Category') !!}
                                                {!! Form::text('category[' . $information->language_id . ']', (old('category[' . $information->language_id . ']') ? old('category[' . $information->language_id . ']') : $information->category ), array('class' => 'form-control')) !!}
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="main-content-block">
                    {!! Form::submit('Save', array('class' => 'btn btn-primary')) !!}
                    <a href="{!! URL::previous() !!}" class="btn btn-default">Cancel</a>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section("script")

    <script type="text/javascript" src="{!! asset('/packages/Dcms/Core/js/bootstrap.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('/packages/Dcms/Core/js/jquery-ui-autocomplete.min.js') !!}"></script>
    <link rel="stylesheet" type="text/css" href="{!! asset('/packages/Dcms/Core/css/jquery-ui-autocomplete.css') !!}">

    <script type="text/javascript" src="{!! asset('/packages/Dcms/Core/ckeditor/ckeditor.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('/packages/Dcms/Core/ckeditor/adapters/jquery.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('/packages/Dcms/Core/ckfinder/ckfinder.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('/packages/Dcms/Core/ckfinder/ckbrowser.js') !!}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            //CKFinder for CKEditor
            CKFinder.setupCKEditor(null, '/ckfinder/');

            //Browser $(this).attr("id")
            $(".browse-server").click(function () {
                var returnid = $(this).attr("id").replace("browse_", "");
                BrowseServer('Images:/', returnid);
            })

            //CKEditor
            $("textarea[id^='description']").ckeditor();
            $("textarea[id^='body']").ckeditor();

            //Bootstrap Tabs
            $(".tab-container .nav-tabs a").click(function (e) {
                e.preventDefault();
                $(this).tab('show');
            })

            //Bootstrap Datepicker
            $(".date").datetimepicker({
                todayHighlight: true,
                autoclose: true,
                pickerPosition: "bottom-left"
            });
        });
    </script>

@stop
